#!/bin/bash
set -e
proxy_addr=$1
no_proxy_addr=$2
internal_connection_check=$3
bootstrap_path=$4

# Example for run
# sudo bash config-proxy.sh 'http://user:pass@proxy.local:8080' 'localhost,127.0.0.1' 'Rodrigo Martinez da Rocha' 'rodrigogrohl@gmail.com' 'http://gitlab.local' 'http://jenkins.local:8080/'
#
if [ ! -z "$bootstrap_path" ]; then echo -e '\033[0;35m'WARNING:'\033[0m' not received bootstrap_path parameter; fi 
proxy_config_file="10proxyconfig"
proxy_check_file="80proxycheck"

export_gitconfig=/home/vagrant/.gitconfig
export_proxy_apt=/etc/apt/apt.conf.d/95proxies
export_proxy_profile=/etc/profile.d/${proxy_config_file}.sh
export_proxy_check=/etc/profile.d/${proxy_check_file}.sh

script_version=0.1.0

headers() {
    echo -e ================== '\033[0;32m'Configuring Proxy'\033[0m' ==================
    echo -e mailto:'\033[0;35m'rodrigogrohl@gmail.com'\033[0m' script version: '\033[0;35m'${script_version}'\033[0m'
    echo `tput rev`Starting Configuration`tput sgr0`
    echo -e Base Address are '\033[0;33m'$proxy_addr '\033[0m'
    echo -e No proxy for '\033[0;31m'$no_proxy_addr '\033[0m'
    echo -e Bootstrap home path '\033[0;31m'$bootstrap_path '\033[0m'
}

headers
echo Configuring: `tput bold`/etc/apt/apt.conf.d/95proxies`tput sgr0`
apt_proxies_template="$(cat ${bootstrap_path}templates/etc-apt-confd-95proxies)"
eval "echo \"${apt_proxies_template}\"" > ${export_proxy_apt}

echo Configuring: `tput bold`/etc/profile.d/${proxy_config_file}.sh`tput sgr0`
profile_proxy="$(cat ${bootstrap_path}templates/etc-profiled-proxyconfig)"
eval "echo \"${profile_proxy}\"" > ${export_proxy_profile}
export {http,https,ftp}_proxy=$proxy_addr
export no_proxy=$no_proxy_addr
export internal_connection_check=$internal_connection_check

apt-get update
#dpkg-query -l tofrodos &> /dev/null
#if [ $? -eq 1 ]; then apt-get install -y tofrodos; fi
apt-get install -y tofrodos

fromdos ${export_proxy_profile}
chmod +x ${export_proxy_profile}

echo Connection Checker: `tput bold`/etc/profile.d/${proxy_check_file}.sh`tput sgr0`
template_proxy_check="$(cat ${bootstrap_path}templates/etc-profiled-proxycheck)"
eval "echo \"${template_proxy_check}\"" > ${export_proxy_check}

fromdos ${export_proxy_check}
chmod +x ${export_proxy_check}
echo -e ================== '\033[0;32m'Config Proxy was done! '\033[0m' ==================
