#!/bin/bash -v -e
echo -e ================== '\033[0;32m'Removing the Proxy'\033[0m' ==================
rm /etc/apt/apt.conf.d/95proxies
# cp /vagrant/bootstrap/proxy/etc-environment /etc/environment
rm /home/vagrant/.gitconfig # Gitconf
rm /etc/profile.d/proxyconfig.sh # Startup exports
export {http,https,ftp}_proxy=''
echo Removing Proxy was done!